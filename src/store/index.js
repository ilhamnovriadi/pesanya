import {createStore} from 'redux'
import cartItem from '../reducers/cartItem.js'

export default store = createStore(cartItem)