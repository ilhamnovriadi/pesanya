import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Header from './Kategori/Molekul/Header';
import Warungterdekat from './Kategori/Molekul/WarungTerdekat';
import Bannervoucher from './Kategori/Molekul/BannerVoucher';
import Carousel from './Kategori/Molekul/Carousel';
import Newshome from './Kategori/Molekul/NewsHome'
import {dummyData} from './Kategori/Molekul/Data';

export default class Home extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <View style={styles.main}>
          <ScrollView>
            <Carousel style={{zIndex: 1}} data={dummyData} />           
            <Warungterdekat/>
            <Bannervoucher />
            <Newshome/>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
  },
  main: {
    flex: 1,
    height: '90%',
  },
});
