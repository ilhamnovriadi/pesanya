import React, {Component, Fragment} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {connect} from 'react-redux'


class DetailProduk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0,
    };
  }
  // change code below this line

  increment = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  decrement = () => {
    if (this.state.count <= 0) {
      this.setState({
        count: 0,
      });
    } else {
      this.setState({
        count: this.state.count - 1,
      });
    }
  };

  reset = () => {};
  render() {
    const {
      namaProd,
      hargaProd,
      ketProd,
      satuanProd,
      diskonProd,
      fotoProd,
      terjual,
      kategoriProd,
      namaWarung,
    } = this.props.route.params.item;
    return (
      <Fragment>
        <View style={styles.header}>
          <View style={styles.containerHeader}>
            <Text style={styles.textJudul}>{namaProd}</Text>
          </View>
        </View>
        <View style={styles.main}>
          <ScrollView>
            <Image
              source={{uri: fotoProd}}
              style={{
                height: 250,
                borderRadius: 10,
                width: '89%',
                alignSelf: 'center',
              }}
              resizeMode="cover"></Image>
            <Text> {hargaProd} </Text>
            <Text> {terjual} </Text>
            <Text style={{paddingHorizontal: 20, paddingTop: 10, fontSize: 15}}>
              {' '}
              {ketProd}{' '}
            </Text>

            <Text> {kategoriProd} </Text>
            <Text> {namaWarung} </Text>
            <Text> {diskonProd} </Text>
          </ScrollView>
          {/* tombol beli */}
          <TouchableOpacity
          activeOpacity={0.8}
            style={{
              position: 'absolute',
              height: 45,
              width: 130,
              backgroundColor: '#ffa41b',
              alignSelf: 'flex-end',
              bottom: 15,
              right: 50,
              borderRadius: 0,
              borderTopRightRadius: 50,
              borderBottomRightRadius: 50,
            }} onPress={()=> this.props.addItemToCart([this.state.count,this.props.route.params.item])}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 10,
              }}>
              <Text
                style={{
                  fontSize: 20,
                  color: '#fff',
                  fontWeight: 'bold',
                  marginRight: 5,
                }}>
                Pesan
              </Text>
              <Icon name="box-open" size={20} color={'#f7f7f7'} />
            </View>
          </TouchableOpacity>
          <View
            style={{
              position: 'absolute',
              height: 45,
              width: 130,
              backgroundColor: '#0779e4',
              alignSelf: 'flex-end',
              bottom: 15,
              right: 180,
              borderTopLeftRadius: 50,
              borderBottomLeftRadius: 50,

            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                paddingVertical: 10,
                justifyContent: 'space-between'
              }}>
              <TouchableOpacity onPress={this.decrement}>
                <Icon name="minus" size={20} color={'#f7f7f7'} style={{paddingLeft: 20}}/>
              </TouchableOpacity>

              <Text
                style={{
                  fontSize: 20,
                  color: '#f7f7f7',
                  fontWeight: 'bold',
                }}>
                {this.state.count}
              </Text>
              <TouchableOpacity onPress={this.increment}>
                <Icon name="plus" size={20} color={'#f7f7f7'} style={{paddingRight: 20}}/>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    marginHorizontal: 0,
    alignItems: 'center',
    justifyContent: 'center',
    height: '10%',
    paddingHorizontal: 20,
  },
  containerHeader: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textJudul: {
    top: -2,
    fontSize: 28,
    fontWeight: 'bold',
    color: '#0779e4',
  },
  main: {
    flex: 1,
    height: '90%',
  },
});

const mapDispatchToProps = (dispatch) => {
return{
  addItemToCart:(product)=>dispatch({type:'TAMBAH_ITEM',payload:product})
}
}
export default connect(null, mapDispatchToProps)(DetailProduk)
