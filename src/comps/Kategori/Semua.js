import React, {Component} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from 'react-native';
import I18n from 'react-native-i18n';
import Icon from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import Header from './Molekul/Header';

const numColom = 2;
const WIDTH = Dimensions.get('window').width;

export default class Semua extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: true,
    };
  }

  formatData = (data, numColom) => {
    const totalBaris = Math.floor(data.length / numColom);
    let totalBarisAkhir = data.length - totalBaris * numColom;

    while (totalBarisAkhir !== 0 && totalBarisAkhir !== numColom) {
      data.push({
        id: 'blank',
        empty: true,
      });
      totalBarisAkhir++;
    }
    return data;
  };

  componentDidMount() {
    this.mounted = true;
    fetch(
      'https://raw.githubusercontent.com/ilhamnovriadi/pesanya2/master/produkMock.json',
    )
      .then((response) => response.json())
      .then((json) => {
        if(this.mounted){
          this.setState({data: json});
        }
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  }

  _renderItem = ({item, index}) => {
    let {textProduk, textHarga, cardBody, cardInvisible} = styles;
    const harga = I18n.toNumber(item.hargaProd, {
      delimiter: '.',
      precision: 0,
    });
    if (item.empty) {
      return <View style={[cardBody, cardInvisible]} />;
    }
    return (
      <TouchableOpacity
      activeOpacity={0.9}
        style={cardBody}
        onPress={() =>
          this.props.navigation.navigate('DetailProduk', {
            item: item
          })
        }>
        <LinearGradient
          colors={['#0779e4', '#0779e4', '#055fb3']}
          style={styles.cardBodyGradient}>
          <View style={styles.fotoCard}>
            <Image
              style={styles.fotoProd}
              source={{
                uri: item.fotoProd,
              }}
            />
          </View>
          <View style={styles.infoCard}>
            <Text style={textProduk} numberOfLines={2} ellipsizeMode="tail">
              {item.namaProd}
            </Text>
          </View>
          <View style={styles.infoHarga}>
            <Text style={textHarga}>
              Rp. {harga}/{item.satuanProd}
            </Text>
          </View>
          <TouchableOpacity style={styles.beliCard}>
            <Icon name="pluscircle" size={35} color="white" />
          </TouchableOpacity>
        </LinearGradient>
      </TouchableOpacity>
    );
  };

  componentWillUnmount(){
    this.mounted = false;
  }
  
  render() {
    const {data, isLoading} = this.state;
    let {container, header, main, textJudul} = styles;
    return (
      <View style={container}>
        <Header />
        <View style={main}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={this.formatData(data, numColom)}
            keyExtractor={(item, id) => id.toString()}
            renderItem={this._renderItem}
            numColumns={numColom}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
  },
  main: {
    height: '90%',
    paddingHorizontal: 10,
  },
  fotoCard: {
    borderRadius: 5,
    height: '60%',
    width: '100%',
    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 12,
    // },
    // shadowOpacity: 0.58,
    // shadowRadius: 1.0,

    // elevation: 5,
  },
  infoCard: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    paddingLeft: 12,
    paddingRight: 50,
  },
  textProduk: {
    paddingTop: 2,
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 1,
    marginTop: 2,
    textAlign: 'left',
    color: '#fff',
  },
  infoHarga: {
    position: 'absolute',
    bottom: 10,
    alignSelf: 'flex-start',
    paddingLeft: 12,
  },
  textHarga: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'white',
  },
  beliCard: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    alignSelf: 'flex-end',
    borderRadius: 50,
    shadowColor: '#000',
    shadowOffset: {width: 0.5, height: 0.5},
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 5,
  },
  textBeli: {
    textAlign: 'center',
    fontSize: 13,
    fontWeight: 'bold',
    paddingTop: 5,
  },
  cardBody: {
    flex: 1,
    marginHorizontal: 9,
    marginVertical: 8,
    height: (WIDTH * 1.1) / numColom,
    borderRadius: 10,
    backgroundColor: '#00263b',
  },
  cardBodyGradient: {
    flex: 1,
    height: (WIDTH * 1.1) / numColom,
    borderRadius: 10,
    backgroundColor: '#00263b',
    shadowColor: '#000',
    shadowOffset: {width: 0.5, height: 0.5},
    shadowOpacity: 0.5,
    shadowRadius: 3,
    elevation: 5,
  },
  cardInvisible: {
    backgroundColor: 'transparent',
  },
  fotoProd: {
    borderRadius: 10,
    borderBottomRightRadius: 0,
    borderBottomLeftRadius: 0,
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
  },
});
