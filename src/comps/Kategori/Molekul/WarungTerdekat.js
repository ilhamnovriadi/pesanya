import React from 'react';
import {StyleSheet, Text, View, ScrollView, Image} from 'react-native';
import banner from '../../../../img/banner2.jpg';
const WarungTerdekat = () => {
  return (
    <View
      style={{
        paddingVertical: 10,
        paddingHorizontal: 17,
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 12,
        }}>
        <Text style={{fontWeight: 'bold', fontSize: 18, color: '#3A98F3'}}>
          Kategori
        </Text>
        <Text style={{fontWeight: 'bold', fontSize: 18, color: '#F1A345'}}>
          Lihat Semua >
        </Text>
      </View>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        style={{flexDirection: 'row'}}>
        <View style={{marginHorizontal: 10, marginLeft: 0, justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{height: 50, width: 120, borderRadius: 8}}
            source={banner}
          />
          <View
            style={{
              height: 50,
              width: 120,
              position: 'absolute',
              backgroundColor: 'black',
              borderRadius: 10,
              opacity: 0.5,
            }}></View>

          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: 'white',
              position: 'absolute'
            }}>
            Sayur
          </Text>
        </View>
        <View style={{marginHorizontal: 10, marginLeft: 0, justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{height: 50, width: 120, borderRadius: 8}}
            source={banner}
          />
          <View
            style={{
              height: 50,
              width: 120,
              position: 'absolute',
              backgroundColor: 'black',
              borderRadius: 10,
              opacity: 0.5,
            }}></View>

          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: 'white',
              position: 'absolute'
            }}>
            Buah
          </Text>
        </View>
        <View style={{marginHorizontal: 10, marginLeft: 0, justifyContent: 'center', alignItems: 'center'}}>
          <Image
            style={{height: 50, width: 120, borderRadius: 8}}
            source={banner}
          />
          <View
            style={{
              height: 50,
              width: 120,
              position: 'absolute',
              backgroundColor: 'black',
              borderRadius: 10,
              opacity: 0.5,
            }}></View>

          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              color: 'white',
              position: 'absolute'
            }}>
            Daging
          </Text>
        </View>
      </ScrollView>
    </View>
  );
};

export default WarungTerdekat;
