import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const BannerVoucher = () => {
  const navigation = useNavigation();
  return (
    <View
      style={{
        paddingHorizontal: 17
      }}>
      <View>
        <Image
          style={{height: 180, width: '100%', borderRadius: 10}}
          source={require('../../../../img/banner2.jpg')}
        />
        <View
          style={{
            height: '100%',
            width: '100%',
            position: 'absolute',
            backgroundColor: 'black',
            borderRadius: 10,
            opacity: 0.5,
          }}></View>
        <Image
          style={{
            height: 30,
            width: 120,
            position: 'absolute',
            bottom: 16,
            left: 18,
          }}
          source={require('../../../../icon/logomini.png')}
        />
        <TouchableOpacity
          onPress={() => navigation.navigate('Semua')}
          title="Belanja"
          style={{
            backgroundColor: '#3A98F3',
            paddingHorizontal: 15,
            paddingVertical: 10,
            position: 'absolute',
            borderRadius: 3,
            bottom: 16,
            right: 16,
          }}>
          <Text style={{fontSize: 11, fontWeight: 'bold', color: 'white'}}>
            Dapatkan Voucher!
          </Text>
        </TouchableOpacity>
        <View
          style={{
            height: 30,
            width: 240,
            position: 'absolute',
            top: 16,
            left: 18,
          }}>
          <Text
            style={{
              fontSize: 35,
              fontWeight: 'bold',
              color: 'white',
              fontStyle: 'italic',
            }}>
            Voucher Rp.200.000,-
          </Text>
        </View>
      </View>
    </View>
  );
};

export default BannerVoucher;
