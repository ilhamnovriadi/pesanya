import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'

const NewsHome = () => {
    return (
        <View style={{marginTop: -15, marginHorizontal: 17}}>
      {/* pembatas1 */}
      <View
        style={{
          marginTop: 15,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            color: '#3A98F3',
            fontSize: 17,
          }}>
          Dapatkan voucher belanjamu!
        </Text>
      </View>
      {/* banner */}
      <View
        style={{
          marginVertical: 16,
          marginBottom: 0,
        }}>
        <View style={{position: 'relative'}}>
          <Image
            style={{height: 180, width: '100%', borderRadius: 10}}
            source={require('../../../../img/banner1.jpg')}
          />
          <Image
            style={{
              height: 30,
              width: 120,
              position: 'absolute',
              bottom: 16,
              left: 18,
            }}
            source={require('../../../../icon/logomini.png')}
          />
          <Image
            style={{
              height: 25,
              width: 100,
              position: 'absolute',
              top: 16,
              right: 16,
            }}
            source={require('../../../../icon/resep1.png')}
          />
        </View>
        <View style={{marginVertical: 10}}>
          <Text
            style={{
              fontSize: 20,
              color: '#3A98F3',
            }}>
            PESANEWS
          </Text>
          <Text
            style={{
              fontSize: 14,
              color: 'grey',
              opacity: 0.8,
              marginBottom: 10,
            }}>
            Bikin rendang dengan rasa baru, rasakan!
          </Text>
          <TouchableOpacity
            style={{
              backgroundColor: '#3A98F3',
              paddingHorizontal: 15,
              paddingVertical: 13,
              alignSelf: 'flex-end',
              borderRadius: 10,
            }}>
            <Text
              style={{
                fontSize: 15,
                fontWeight: 'bold',
                color: 'white',
              }}>
              Baca
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* __________________________________________________ */}
    </View>
    )
}

export default NewsHome
