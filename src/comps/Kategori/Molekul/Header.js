import React, {Component} from 'react';
import {Text, StyleSheet, View, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.header}>
        <View style={styles.containerHeader}>
          <Text style={styles.textJudul}>Pesanya</Text>
          {/* <View style={styles.cariBar}>
            <Icon
              style={{left: 10, top: 8}}
              name="search"
              size={23}
              color="grey"
            />
            <TextInput
              style={styles.inputCari}
              placeholder="Telor Ayam"></TextInput>
          </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    marginHorizontal: 0,
    alignItems: 'center',
    justifyContent: 'center',
    height: '10%',
    paddingHorizontal: 20
  },
  containerHeader: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cariBar: {
    alignSelf: 'center',
    backgroundColor: '#f7f7f7',
    borderRadius: 8,
    width: '60%',
    flexDirection: 'row',
    borderWidth: 2,
    borderColor: '#dddddd',
  },
  inputCari: {
    paddingVertical: 8,
    paddingLeft: 20,
    paddingRight: 35,
    fontSize: 18,
    fontWeight: 'bold',
    height: 40,
    width: '100%',
  },
  textJudul: {
    top: -2,
    fontSize: 28,
    fontWeight: 'bold',
    color: '#0779e4',
  },
});
