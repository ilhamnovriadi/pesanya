import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {connect} from 'react-redux';
//import CartProduk from './Kategori/Molekul/CartProduk.js'
import LinearGradient from 'react-native-linear-gradient';
import I18n from 'react-native-i18n';

class Keranjang extends Component {
  constructor(props) {
    super(props);

    this.state = {
      datacard: [],
      sumHarga: []
    };
  }

  // componentDidMount() {
  //   AsyncStorage.getItem('cart').then((cart) => {
  //       console.log(cart)
  //     if (cart !== null) {
  //       const cartfood = JSON.parse(cart);
  //       this.setState = {
  //         datacard: cartfood,
  //       };
  //     }
  //   });
  // }

  _renderItem = ({item, index}) => {
    const [banyak, {id, namaProd, hargaProd, fotoProd}] = item;
    const harga = I18n.toNumber(hargaProd, {
      delimiter: '.',
      precision: 0,
    });
    return (
      <TouchableOpacity activeOpacity={0.9}>
        <LinearGradient
          colors={['#0779e4', '#0779e4', '#055fb3']}
          style={{padding: 10}}>
          <Image source={{uri: fotoProd}} style={{width: 50, height: 50}} />
          <Text>{banyak} Kg</Text>
          <Text>{namaProd}</Text>
          <Text>Rp. {harga}</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  };

  render() {
    const getHeader = () => {
      return <Text>Total</Text>;
    };
    return (
      <View style={styles.container}>
        <Text style={{fontSize: 30, fontWeight: 'bold', paddingTop: 10}}>
          KERANJANG
        </Text>
        <SafeAreaView style={{flex: 1}}>
          {this.props.cartItem.length < 1 ? (
            <Text>No Data</Text>
          ) : (
            <FlatList
              data={this.props.cartItem}
              keyExtractor={(item, id) => id.toString()}
              renderItem={this._renderItem}
              ListFooterComponent={getHeader}
            />
          )}
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#d7d7d7',
  },
});

const mapStateToProps = (state) => {
  return {
    cartItem: state,
  };
};

export default connect(mapStateToProps)(Keranjang);
