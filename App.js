import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
//import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from './src/comps/Home';
import Keranjang from './src/comps/Keranjang2';
import Akun from './src/comps/Akun';
import Pesanan from './src/comps/Pesanan';
import Semua from './src/comps/Kategori/Semua';
import News from './src/comps/Kategori/News';
//import Chat from './src/comps/Chat';
import DetailProduk from './src/comps/Kategori/DetailProduk';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {Provider} from 'react-redux';
import store from './src/store';

const Tab = createMaterialBottomTabNavigator();

//const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    createBelanjaStack = () => (
      <Stack.Navigator screenOptions={{animationEnabled: false}}>
        <Stack.Screen
          name="Belanja"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Semua"
          component={Semua}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DetailProduk"
          component={DetailProduk}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="News"
          component={News}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );

    return (
      <Provider store={store}>
        <NavigationContainer>
          <Tab.Navigator
            barStyle={{backgroundColor: '#0779e4'}}
            activeColor="#f6d743">
            <Tab.Screen
              name="Belanja"
              children={createBelanjaStack}
              options={{
                tabBarLabel: 'Belanja',
                tabBarIcon: ({color}) => (
                  <Icon name="shopping-bag" size={20} color={color} />
                ),
              }}
            />
            {/* <Tab.Screen
            name="Chat"
            component={Chat}
            options={{
              tabBarLabel: 'Chat',
              tabBarIcon: ({color}) => (
                <Icon2 name="comments" size={20} color={color} />
              ),
            }}
          /> */}
            <Tab.Screen
              name="Pesanan"
              component={Pesanan}
              options={{
                tabBarLabel: 'Pesanan',
                tabBarIcon: ({color}) => (
                  <Icon name="shipping-fast" size={20} color={color} />
                ),
              }}
            />
            <Tab.Screen
              name="Keranjang"
              component={Keranjang}
              options={{
                tabBarLabel: 'Keranjang',
                tabBarIcon: ({color}) => (
                  <Icon name="box-open" size={19} color={color} />
                ),
              }}
            />
            <Tab.Screen
              name="Akun"
              component={Akun}
              options={{
                tabBarLabel: 'Akun',
                tabBarIcon: ({color}) => (
                  <Icon name="user-cog" size={18} color={color} />
                ),
              }}
            />
          </Tab.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}
